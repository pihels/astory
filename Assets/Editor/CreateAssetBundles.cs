﻿using System.IO;
using UnityEditor;
using UnityEngine;
public class CreateAssetBundles {
    [MenuItem ("Assets/Build AssetBundles")]
    static void BuildAllAssetBundles () {
        string dir = "Assets/StreamingAssets";
        if (!Directory.Exists (Application.streamingAssetsPath)) {
            Directory.CreateDirectory (dir);
        }

        BuildPipeline.BuildAssetBundles (dir, BuildAssetBundleOptions.None, EditorUserBuildSettings.activeBuildTarget);
    }
}