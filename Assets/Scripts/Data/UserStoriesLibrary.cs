﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (fileName = "UserStoriesLibrary", menuName = "AStory/Libraries/UserStoriesLibrary", order = 1)]
public class UserStoriesLibrary : ScriptableObject {

    [SerializeField]
    public List<UserStory> stories = new List<UserStory> ();

    public void AddStory (UserStory story) {
        stories.Add (story);
    }

    public void UpdateStory (UserStory story) {
        stories[stories.IndexOf (story)] = story;
    }

    public void DeleteStory (UserStory story) {
        stories.Remove (story);
    }

    public void Clear () {
        stories.Clear ();
    }
}