﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

[System.Serializable]
public class UserStory {
    public List<UserStoryData> data;
    public int templateId;
    public int backgroundIdx;
    public int firstCharIdx;
    public int secondCharIdx;
    public bool isFinished;

    public AudioClip clip;
}

[System.Serializable]
public class UserStoryData {
    public string text;
    public AudioClip audio;
    public string audioFileName;
}