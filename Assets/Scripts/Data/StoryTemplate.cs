﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (fileName = "StoryTemplate", menuName = "AStory/StoryTemplate", order = 1)]
public class StoryTemplate : ScriptableObject {
    public int id;
    public int length;
    public Sprite thumbnail;
    public StoryTemplateElement[] backgrounds;
    public StoryTemplateElement[] leftElements;
    public StoryTemplateElement[] rightElements;
    public List<GameObject> animations;

    public Sprite GetLeftThumbnail (bool isColor, int setIdx) {
        return GetThumbnail (isColor, setIdx, leftElements);
    }

    public Sprite GetRightThumbnail (bool isColor, int setIdx) {
        return GetThumbnail (isColor, setIdx, rightElements);
    }

    public Sprite GetBgThumbnail (bool isColor, int setIdx) {
        return GetThumbnail (isColor, setIdx, backgrounds);
    }

    public Sprite GetLeftSprite (bool isColor, int setIdx, int sceneIdx) {
        return GetSprite (isColor, setIdx, sceneIdx, leftElements);
    }

    public Sprite GetRightSprite (bool isColor, int setIdx, int sceneIdx) {
        return GetSprite (isColor, setIdx, sceneIdx, rightElements);
    }

    public Sprite GetBackgroundSprite (bool isColor, int setIdx, int sceneIdx) {
        return GetSprite (isColor, setIdx, sceneIdx, backgrounds);
    }

    private Sprite GetSprite (bool isColor, int setIdx, int sceneIdx, StoryTemplateElement[] sets) {
        if (!isColor) {
            return sets[setIdx].bwImages[sceneIdx];
        }
        return sets[setIdx].colorImages[sceneIdx];
    }

    private Sprite GetThumbnail (bool isColor, int setIdx, StoryTemplateElement[] sets) {
        if (!isColor) {
            return sets[setIdx].bwThumbnail;
        }
        return sets[setIdx].colorThumbnail;
    }
}