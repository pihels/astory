﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (fileName = "StoryTemplatesLibrary", menuName = "AStory/Libraries/StoryTemplatesLibrary", order = 1)]
public class StoryTemplatesLibrary : ScriptableObject {
    public StoryAssetBundle[] bundles;
}

[System.Serializable]
public class StoryAssetBundle {
    public int id;
    public Sprite thumbnail;
    public string name;
}