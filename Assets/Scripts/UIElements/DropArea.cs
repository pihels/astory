﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class DropArea : MonoBehaviour, IDropHandler {

    public int index;

    public void OnDrop (PointerEventData eventData) {
        DraggableStoryElement draggable = eventData.pointerDrag.GetComponent<DraggableStoryElement> ();
        if (draggable == null) {
            return;
        }

        if (draggable.sceneIdx == index) {
            ImageHelper.SetAndStretchToParentSize (draggable.GetComponent<RectTransform> (), this.GetComponent<RectTransform> ());
            draggable.Lock ();
            SwitchableStoryManager.Instance.OnStoryElementLock (draggable);
        }

    }

    public void LockElement (DraggableStoryElement el) {
        ImageHelper.SetAndStretchToParentSize (el.GetComponent<RectTransform> (), this.GetComponent<RectTransform> ());
        el.Lock ();
    }

}