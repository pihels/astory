﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
public class LibraryPanel : MonoBehaviour {

    public GameObject[] buttons;
    public GameObject prevBtn;
    public GameObject nextBtn;
    public GameObject dragImage;

    private bool isTemplatesLibrary;
    private List<StoryAssetBundle> bundles;
    private int startIdx;
    private bool isDragging;

    void Awake () {
        if (dragImage != null) {
            dragImage.SetActive (false);
        }
    }

    void Update () {
        if (isDragging) {
            Debug.Log (Input.mousePosition);
            dragImage.GetComponent<RectTransform> ().position = Input.mousePosition;
        }
    }

    public void Init (UserStoriesLibrary library) {
        isTemplatesLibrary = false;
        startIdx = 0;
        bundles = new List<StoryAssetBundle> ();
        foreach (UserStory story in library.stories) {
            bundles.Add (AppManager.Instance.GetAssetBundle (story.templateId));
        }

        InitLibrary ();
    }

    public void Init (StoryTemplatesLibrary library) {
        isTemplatesLibrary = true;
        startIdx = 0;
        bundles = new List<StoryAssetBundle> (library.bundles);

        InitLibrary ();
    }

    private void InitLibrary () {
        ToggleNavButtons ();

        int i = startIdx;
        foreach (var btn in buttons) {
            Image image = btn.GetComponent<Image> ();

            if (i < bundles.Count) {
                StoryAssetBundle bundle = bundles[i];
                image.sprite = bundle.thumbnail;
                i++;
                btn.transform.GetChild (2).gameObject.SetActive (false);
                btn.transform.GetChild (1).gameObject.SetActive (false);
                btn.GetComponent<Button> ().interactable = true;
            } else {
                btn.transform.GetChild (2).gameObject.SetActive (true);
                btn.transform.GetChild (1).gameObject.SetActive (true);
                btn.GetComponent<Button> ().interactable = false;
            }
        }
    }

    public void ShowPrevBatch () {
        startIdx -= buttons.Length;
        if (startIdx < 0) {
            startIdx = 0;
        }

        InitLibrary ();
    }

    public void ShowNextBatch () {
        startIdx += buttons.Length;
        InitLibrary ();
    }

    private void ToggleNavButtons () {
        prevBtn.SetActive (startIdx != 0);
        nextBtn.SetActive (startIdx + buttons.Length < bundles.Count);
    }

    public void OpenStory (int idx) {
        if (isTemplatesLibrary) {
            StoriesLibraryManager.Instance.StartNewUserStory (startIdx + idx);
        } else {
            StoriesLibraryManager.Instance.OpenUserStory (startIdx + idx);
        }
    }

    public void OnBeginDrag (int idx) {
        Debug.Log ("being drag");
        isDragging = true;
        dragImage.SetActive (true);
        dragImage.GetComponent<Image> ().sprite = bundles[idx].thumbnail;
    }

    public void OnEndDrag (int idx) {
        Debug.Log ("end drag");
        dragImage.SetActive (false);
        isDragging = false;

        var pointerEventData = new PointerEventData (EventSystem.current);
        pointerEventData.position = Input.mousePosition;
        var results = new List<RaycastResult> ();
        EventSystem.current.RaycastAll (pointerEventData, results);

        //For every result returned, output the name of the GameObject on the Canvas hit by the Ray
        foreach (RaycastResult result in results) {
            if (result.gameObject.tag == "TrashCan") {
                StoriesLibraryManager.Instance.DeleteUserStory (startIdx + idx);
            }
            Debug.Log ("Hit " + result.gameObject.name);
        }
    }
}