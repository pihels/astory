﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.Events;
public class DraggableStoryElement : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler {
    public UnityEvent beginDragEvent = new UnityEvent();
    public GameObject imageGo;
    public GameObject ghostGo;
    public Animator overlay;
    public int sceneIdx;

    public bool isLocked = false;
    private Vector3 startPos;
    private Vector2 startSize;

    void Awake () {
        ghostGo.SetActive (false);

        startPos = this.transform.position;
        startSize = this.GetComponent<RectTransform> ().sizeDelta;
    }

    public void OnBeginDrag (PointerEventData eventData) {
        if (isLocked) {
            return;
        }

        beginDragEvent.Invoke();
        ghostGo.transform.position = transform.position;
        ghostGo.SetActive (true);
    }

    public void OnDrag (PointerEventData eventData) {
        ghostGo.transform.position += (Vector3) eventData.delta;
    }

    public void OnEndDrag (PointerEventData eventData) {
        ghostGo.SetActive (false);
    }

    public void Reset () {
        this.transform.position = startPos;
        this.GetComponent<RectTransform> ().sizeDelta = startSize;
    }

    public void Lock () {
        isLocked = true;
    }
}