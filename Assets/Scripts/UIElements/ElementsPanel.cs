﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class ElementsPanel : MonoBehaviour {

    public GameObject char1Btn;
    public GameObject char2Btn;
    public GameObject backgroundBtn;
    public GameObject startStoryBtn;
    public GameObject elementSelectionPanel;
    public GameObject[] selectionBtns;

    public GameSettings settings;
    public UserStory userStory;

    private int currentSelection = 0;
    private StoryTemplate template;
    
    public void Init (StoryTemplate template) {
        DisableButtons ();
        char1Btn.SetActive (true);
        userStory = new UserStory ();
        userStory.templateId = template.id;
        userStory.data = new List<UserStoryData>();
        AppManager.Instance.currentStory = userStory;
        AppManager.Instance.currentTemplate = template;

        this.template = template;
    }

    private void DisableButtons () {
        backgroundBtn.SetActive (false);
        char1Btn.SetActive (false);
        char2Btn.SetActive (false);
        startStoryBtn.SetActive (false);
    }

    public void OpenChar1Select () {
        currentSelection = 0;
        elementSelectionPanel.SetActive (true);
        InitSelectionBtns (template.leftElements);
    }

    public void OpenChar2Select () {
        currentSelection = 1;
        elementSelectionPanel.SetActive (true);
        InitSelectionBtns (template.rightElements);
    }
    public void OpenBackgroundSelect () {
        currentSelection = 2;
        elementSelectionPanel.SetActive (true);
        InitSelectionBtns (template.backgrounds);
    }

    public void StartStory () {
        AppManager.Instance.userLibrary.AddStory (userStory);
        SceneManager.LoadScene ("ChangableStory");
    }

    private void InitSelectionBtns (StoryTemplateElement[] elements) {
        List<Sprite> sprites = new List<Sprite> ();
        foreach (var el in elements) {
            if (settings.isColored) {
                sprites.Add (el.colorThumbnail);
            } else {
                sprites.Add (el.bwThumbnail);
            }
        }

        int i = 0;
        foreach (var btn in selectionBtns) {
            Image image = btn.GetComponent<Image> ();
            if (i < sprites.Count) {
                image.sprite = sprites[i];
                i++;
                btn.SetActive (true);
            } else {
                btn.SetActive (false);
            }
        }

    }

    public void SelectElement (int idx) {
        elementSelectionPanel.SetActive (false);
        switch (currentSelection) {
            case 0:
                userStory.firstCharIdx = idx;
                char1Btn.GetComponent<Image> ().sprite = template.GetLeftThumbnail (settings.isColored, idx);
                char2Btn.SetActive (true);
                break;
            case 1:
                userStory.secondCharIdx = idx;
                char2Btn.GetComponent<Image> ().sprite = template.GetRightThumbnail (settings.isColored, idx);
                backgroundBtn.SetActive (true);
                break;
            case 2:
                userStory.backgroundIdx = idx;
                backgroundBtn.GetComponent<Image> ().sprite = template.GetBgThumbnail (settings.isColored, idx);
                startStoryBtn.SetActive (true);
                break;
        }
    }
}