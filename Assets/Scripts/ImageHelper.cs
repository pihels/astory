﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public static class ImageHelper {

    public static void InitStoryElements (StoryTemplate story, GameObject elementTemplate, List<DraggableStoryElement> elements) {
        for (int sceneIdx = 0; sceneIdx < story.length; sceneIdx++) {
            GameObject element = GameObject.Instantiate (elementTemplate);
            element.name = sceneIdx + " " + element.name;
            DraggableStoryElement draggable = element.GetComponent<DraggableStoryElement> ();
            CreateStoryImages (draggable, story, sceneIdx);
            elements.Add (draggable);
        }
    }

    private static void CreateStoryImages (DraggableStoryElement draggable, StoryTemplate story, int sceneIdx) {
        UserStory userStory = AppManager.Instance.currentStory;
        draggable.sceneIdx = sceneIdx;
        Sprite bg = story.GetBackgroundSprite (true, userStory.backgroundIdx, sceneIdx);
        Sprite left = story.GetLeftSprite (true, userStory.firstCharIdx, sceneIdx);
        Sprite right = story.GetRightSprite (true, userStory.secondCharIdx, sceneIdx);

        SetSprite (draggable, "Background", bg);
        SetSprite (draggable, "LeftCharacter", left);
        SetSprite (draggable, "RightCharacter", right);
    }

    private static void SetSprite (DraggableStoryElement draggable, string childName, Sprite sprite) {
        Image image = draggable.imageGo.transform.Find (childName).GetComponent<Image> ();
        Image ghostImage = draggable.ghostGo.transform.Find (childName).GetComponent<Image> ();
        if (sprite != null) {
            image.enabled = true;
            image.sprite = sprite;
            ghostImage.enabled = true;
            ghostImage.sprite = sprite;
        } else {
            image.enabled = false;
            ghostImage.enabled = false;
        }
    }

    public static void SetHolderImage (RectTransform imageHolder, DraggableStoryElement element) {
        element.gameObject.SetActive (true);
        ImageHelper.SetAndStretchToParentSize (element.GetComponent<RectTransform> (), imageHolder);
    }

    public static void ShuffleElements (List<DraggableStoryElement> elements) {
        if (elements.Count == 0) {
            return;
        }
        elements.Sort ((a, b) => Random.Range (0, 2) * 2 - 1);
        //TODO better system to avoid correct order shuffling
        if (elements[0].sceneIdx == 0 && elements[1].sceneIdx == 1 &&
            elements[2].sceneIdx == 2 && elements[3].sceneIdx == 3) {
            ShuffleElements (elements);
        }
    }

    public static void SetAndStretchToParentSize (RectTransform _mRect, RectTransform _parent) {
        _mRect.transform.SetParent (_parent);
        _mRect.SetSiblingIndex(1);
        _mRect.anchoredPosition = new Vector2 (0, 0);
        _mRect.anchorMin = new Vector2 (0, 0);
        _mRect.anchorMax = new Vector2 (1, 1);
        _mRect.pivot = new Vector2 (0.5f, 0.5f);
        _mRect.sizeDelta = new Vector2 (0, 0);
        _mRect.localScale = new Vector3 (1f, 1f, 1f);
    }

    public static void RandomizeSiblingOrder (Transform parent) {
        int childCount = parent.childCount;
        foreach (Transform child in parent) {
            int idx = Random.Range (0, childCount);
            child.SetSiblingIndex (idx);
        }
    }

}