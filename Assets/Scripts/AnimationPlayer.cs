﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationPlayer : MonoBehaviour {
    private const float MIN_DISPLAY_TIME = 5.0F;
    private const float WORD_READ_TIME = 1.5F;

    public Camera myCamera;
    public AudioSource audioSource;
    public GameObject animationHolder;
    private List<GameObject> animationScenes;
    private GameObject currentScene;
    private int curPlaybackIdx = -1;
    private bool isWithRecordings;
    private float startPlayTime = 0;
    private float currentSceneLength = 0;
    private float currentPlaytime = 0;

    public void Init (bool isWithRecordings) {
        List<GameObject> animationScenePrefabs = AppManager.Instance.currentTemplate.animations;
        AppManager.Instance.DisableBackground ();
        this.animationScenes = new List<GameObject> ();
        this.isWithRecordings = isWithRecordings;

        Debug.Log (myCamera.aspect);

        foreach (GameObject scenePrefab in animationScenePrefabs) {
            GameObject scene = Instantiate (scenePrefab, Vector3.zero, scenePrefab.transform.rotation);
            if (myCamera.aspect < 1.5) {
                scene.transform.localScale *= 0.7f;
            }
            scene.SetActive (false);
            animationScenes.Add (scene);
        }
    }

    private void PlayAnimation () {
        ToggleShownAnimation ();
        currentScene = animationScenes[curPlaybackIdx];
        currentSceneLength = GetAnimationDisplayTime ();
        ResumeAnimation ();

        if (!isWithRecordings) {
            return;
        }

        UserStoryData data = AppManager.Instance.currentStory.data[curPlaybackIdx];

        if (data.audio != null) {
            audioSource.clip = data.audio;
            audioSource.Play ();
        }
    }

    private float GetAnimationDisplayTime () {
        if (!isWithRecordings) {
            //todo add struct to animation gameobject that holds duration
            return MIN_DISPLAY_TIME;
        }

        UserStoryData data = AppManager.Instance.currentStory.data[curPlaybackIdx];

        if (data.audio != null) {
            return data.audio.length;
        }

        if (data.text != null) {
            int wordCount = data.text.Split (' ').Length - 1;
            return wordCount * WORD_READ_TIME;
        }

        return MIN_DISPLAY_TIME;
    }

    private void ToggleShownAnimation () {
        int i = 0;
        foreach (GameObject scene in animationScenes) {
            scene.SetActive (i == curPlaybackIdx);
            i++;
        }
    }

    public void PlayNextAnimation () {
        curPlaybackIdx++;
        PlayAnimation ();
    }

    public void PlayPrevAnimation () {
        curPlaybackIdx--;
        PlayAnimation ();
    }

    public void PauseAnimation () {
        if (currentScene == null) {
            return;
        }

        currentPlaytime = Time.time - startPlayTime;
        foreach (Animator a in currentScene.GetComponentsInChildren<Animator> ()) {
            a.enabled = false;
        }

        if (isWithRecordings) {
            audioSource.Pause ();
        }
    }

    public void ResumeAnimation () {
        if (currentScene == null) {
            return;
        }

        startPlayTime = Time.time;
        foreach (Animator a in currentScene.GetComponentsInChildren<Animator> ()) {
            a.enabled = true;
        }

        if (isWithRecordings) {
            audioSource.Play ();
        }
    }

    public int GetCurrentAnimationIndex () {
        return curPlaybackIdx;
    }

    public float GetCurrentSceneLength () {
        return currentSceneLength;
    }

    public float GetCurrentSceneRemainingLength () {
        float result = currentSceneLength - currentPlaytime;
        return result < 0 ? 0 : result;
    }

    public bool IsLastAnimation () {
        return curPlaybackIdx == animationScenes.Count - 1;
    }

    public bool IsFirstAnimation () {
        return curPlaybackIdx == 0;
    }
}