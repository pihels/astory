﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopiMenu : MonoBehaviour {
    public MenuManager menuManager;
    public AudioSource audioSource;
    public AudioClip clip;

    private Animator animator;
    private bool hasWokenUp;
    private int popiClickCount = 0;

    void Start () {
        animator = GetComponent<Animator> ();
    }
    public void OnPopiClick () {
        if (hasWokenUp) {
            return;
        }
        AppManager.Instance.hasStarted = true;
        popiClickCount++;

        if (popiClickCount < 3) {
            animator.Play ("popi_pretend_wakeup");
        } else {
            StartCoroutine (SetIdle ());
            audioSource.clip = clip;
            audioSource.Play ();
            animator.SetBool ("IsSpeaking", true);
            animator.Play ("popi_wakeup");
            menuManager.ShowMenuButtons ();
            hasWokenUp = true;
        }
    }

    private IEnumerator SetIdle () {
        yield return new WaitForSeconds (clip.length);
        animator.SetBool ("IsSpeaking", false);
        menuManager.ShowIdlePopi ();
    }
}