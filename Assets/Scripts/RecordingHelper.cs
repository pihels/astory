﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RecordingHelper : MonoBehaviour {
    public AudioSource audioSource;
    public bool isRecording;
    private List<float> tempRecording = new List<float> ();
    private AudioClip lastRecording;

    private string microphone;
    private int freq = 16000;
    private int clipLength = 300;

    void Start () {
        SetMicrophone ();
    }

    public int GetFreq () {
        return freq;
    }

    private void SetMicrophone () {
        foreach (string device in Microphone.devices) {
            int minFrequency;
            int maxFrequency;
            Microphone.GetDeviceCaps (device, out minFrequency, out maxFrequency);
            Debug.LogWarning ("ASTORY found microphone device " + device + " min " + minFrequency + " max " + maxFrequency);
            if (string.IsNullOrEmpty (microphone)) {
                //set default mic to first mic found.
                microphone = device;
                freq = maxFrequency > 0 ? maxFrequency : 44100;
                return;
            }
        }
    }

    public void StartRecording () {
        if (string.IsNullOrEmpty (microphone)) {
            SetMicrophone ();
        }

        isRecording = true;
        audioSource.Stop ();
        tempRecording.Clear ();
        Microphone.End (microphone);

        audioSource.clip = Microphone.Start (microphone, true, clipLength, freq);
        while (!(Microphone.GetPosition (microphone) > 1)) {
            // Wait until the recording has started
            UnityEngine.Debug.Log ("recording not started");
        }

        UnityEngine.Debug.Log ("recording started");
        Invoke ("ResizeRecording", clipLength);
    }

    private void ResizeRecording () {
        if (!isRecording) {
            return;
        }

        //add the next second of recorded audio to temp vector
        int length = clipLength * freq;
        float[] clipData = new float[length];
        audioSource.clip.GetData (clipData, 0);
        tempRecording.AddRange (clipData);
        Invoke ("ResizeRecording", clipLength);
    }

    public AudioClip EndRecording () {
        //stop recording, get length, create a new array of samples
        int length = Microphone.GetPosition (microphone);

        Microphone.End (microphone);
        float[] clipData = new float[length];
        audioSource.clip.GetData (clipData, 0);

        //create a larger vector that will have enough space to hold our temporary
        //recording, and the last section of the current recording
        float[] fullClip = new float[clipData.Length + tempRecording.Count];
        for (int i = 0; i < fullClip.Length; i++) {
            //write data all recorded data to fullCLip vector
            if (i < tempRecording.Count)
                fullClip[i] = tempRecording[i];
            else
                fullClip[i] = clipData[i - tempRecording.Count];
        }

        lastRecording = AudioClip.Create ("Jutt", fullClip.Length, 1, freq, false);
        lastRecording.SetData (fullClip, 0);
        isRecording = false;
        //Debug.Log ("recording length " + lastRecording.length);
        return lastRecording;
    }

    public AudioClip GetLastRecording () {
        return lastRecording;
    }
}