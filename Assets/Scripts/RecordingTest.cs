﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class RecordingTest : MonoBehaviour {
    public RecordingHelper recordingHelper;

    public GameObject recordBtn;
    public GameObject playBtn;
    public AudioSource audioSource;
    public InputField freqField;
    public Text freqText;

    private AudioClip currentRecording;

    IEnumerator Start () {
        Debug.Log("waiting for recording permission");
        yield return Application.RequestUserAuthorization (UserAuthorization.Microphone);
        Debug.Log("recording permission granted");
    }
    
    void Update () {
        freqText.text = recordingHelper.GetFreq().ToString ();
    }
    public void Record () {
        Debug.Log ("record");
        if (!recordingHelper.isRecording) {
            recordingHelper.StartRecording ();
            recordBtn.GetComponent<Image> ().color = Color.red;
            playBtn.SetActive (false);
        } else {
            currentRecording = recordingHelper.EndRecording ();
            recordBtn.GetComponent<Image> ().color = Color.white;
            playBtn.SetActive (true);
        }
    }

    public void Play () {
        audioSource.clip = currentRecording;
        audioSource.Play ();
    }

    public void ChangeFreq () {
        //recordingHelper.GetFreq() = int.Parse (freqField.text);
    }

}