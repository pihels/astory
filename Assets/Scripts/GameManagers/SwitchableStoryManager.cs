﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class SwitchableStoryManager : Singleton<SwitchableStoryManager> {

    public GameObject gameCanvas;
    public GameObject elementTemplate;
    public AudioSource audioSource;
    public RectTransform dropAreasHolder;
    public RectTransform[] imageHolders;
    public GameObject dropAreaPrefab;

    private List<DraggableStoryElement> storyElements = new List<DraggableStoryElement> ();
    private List<DraggableStoryElement> remainingElements = new List<DraggableStoryElement> ();
    private List<DropArea> dropAreas = new List<DropArea> ();
    public Animator firstElementAnimator;
    private bool isStoryOrdered;
    private bool hasFirstElementMoved;

    void Start () {
        Init ();
    }

    private void Init () {
        InitDropAreas ();
        ClearElements ();
        ImageHelper.InitStoryElements (AppManager.Instance.currentTemplate, elementTemplate, storyElements);
        foreach (DraggableStoryElement el in storyElements) {
            remainingElements.Add (el);
        }
        ImageHelper.ShuffleElements (remainingElements);
        InitUI ();
    }

    private void ClearElements () {
        foreach (DraggableStoryElement el in storyElements) {
            Destroy (el.gameObject);
        }

        storyElements.Clear ();
        remainingElements.Clear ();
    }

    private void InitDropAreas () {
        for (int i = 0; i < AppManager.Instance.currentTemplate.length; i++) {
            GameObject dropAreaGo = GameObject.Instantiate (dropAreaPrefab, dropAreasHolder);
            if (AppManager.Instance.currentTemplate.length == 5) {
                dropAreaGo.transform.localScale *= 0.9f;
            }
            DropArea dropArea = dropAreaGo.GetComponent<DropArea> ();
            dropAreas.Add (dropArea);
            dropArea.index = i;
            Text dropAreaText = dropAreaGo.GetComponentInChildren<Text> ();
            dropAreaText.text = (i + 1).ToString ();
        }
    }

    private void InitUI () {
        if (remainingElements.Count == 0) {
            return;
        }

        int i = 0;
        foreach (RectTransform holder in imageHolders) {
            if (i < remainingElements.Count) {
                DraggableStoryElement el = remainingElements[i];
                ImageHelper.SetHolderImage (holder, el);
                if (el.sceneIdx == 0) {
                    firstElementAnimator = el.overlay;
                    el.beginDragEvent.RemoveListener (OnElementMoved);
                    el.beginDragEvent.AddListener (OnElementMoved);
                }
            } else {
                holder.gameObject.SetActive (false);
            }
            i++;
        }

        StartCoroutine (HighlightFirstElement ());
    }

    private IEnumerator HighlightFirstElement () {
        yield return new WaitForSeconds (12.0f);

        if (!hasFirstElementMoved && firstElementAnimator != null) {
            firstElementAnimator.SetBool ("IsActive", true);
        }
    }

    private void OnElementMoved () {
        hasFirstElementMoved = true;
        if (firstElementAnimator != null) {
            firstElementAnimator.SetBool ("IsActive", false);
        }
    }

    private void StartAnimationPlayback () {
        AppManager.Instance.LoadScene ("StoryAnimationScene");
    }

    public bool IsStoryOrdered () {
        return remainingElements.Count == 0;
    }

    public void PlayAudio (AudioClip audio) {
        audioSource.clip = audio;
        audioSource.Play ();
    }

    public void OnStoryElementLock (DraggableStoryElement element) {
        remainingElements.Remove (element);
        isStoryOrdered = IsStoryOrdered ();

        if (isStoryOrdered) {
            StartAnimationPlayback ();
        }
    }

}