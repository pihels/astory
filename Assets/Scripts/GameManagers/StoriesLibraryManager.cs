﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StoriesLibraryManager : Singleton<StoriesLibraryManager> {

    public GameObject userLibraryPanel;
    public GameObject templateLibraryPanel;
    public GameObject elementsPanel;
    public GameObject clearBtn;

    public bool isTemplates;

    protected override void OnAwake () {
        OpenLibraryPanel ();
        clearBtn.SetActive (AppManager.Instance.userLibrary.stories.Count > 0);
    }

    public void OpenLibraryPanel () {
        DeactivatePanels ();
        userLibraryPanel.SetActive (true);

        if (isTemplates) {
            userLibraryPanel.GetComponent<LibraryPanel> ().Init (AppManager.Instance.templatesLibrary);
        } else {
            userLibraryPanel.GetComponent<LibraryPanel> ().Init (AppManager.Instance.userLibrary);
        }
    }

    public void OpenUserStory (int idx) {
        AppManager.Instance.SetCurrentStory (AppManager.Instance.userLibrary.stories[idx]);
        AppManager.Instance.LoadScene ("StoryAnimationWithRecordingSceneWithoutPopi");
    }

    public void StartNewUserStory (int idx) {
        StoryTemplate template = AppManager.Instance.GetTemplate (idx);
        if (template == null) {
            return;
        }

        UserStory userStory = new UserStory ();
        userStory.templateId = template.id;
        userStory.data = new List<UserStoryData> ();
        for (int i = 0; i < template.length; i++) {
            userStory.data.Add (new UserStoryData ());
        }
        AppManager.Instance.SetCurrentStory (userStory, template);
        AppManager.Instance.LoadScene ("StoryPreviewScene");
    }

    private void DeactivatePanels () {
        userLibraryPanel.SetActive (false);
        templateLibraryPanel.SetActive (false);
        elementsPanel.SetActive (false);
    }

    public void ClearLibrary () {
        AppManager.Instance.userLibrary.Clear ();
        AppManager.Instance.SaveData ();
        OnAwake ();
    }

    public void DeleteUserStory (int idx) {
        AppManager.Instance.userLibrary.DeleteStory (AppManager.Instance.userLibrary.stories[idx]);
        AppManager.Instance.SaveData ();
        OnAwake ();
    }
}