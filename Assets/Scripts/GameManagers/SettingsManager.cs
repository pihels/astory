﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SettingsManager : Singleton<SettingsManager> {


    public GameObject colorStyleBtn;
    public GameObject bwStyleBtn;

    public GameSettings settings;

    protected override void OnAwake () {
        ToggleArtStyleBtns ();
    }

    public void SetBwStyle () {
        settings.isColored = false;
        ToggleArtStyleBtns ();
    }

    public void SetColoredStyle () {
        settings.isColored = true;
        ToggleArtStyleBtns ();
    }

    private void ToggleArtStyleBtns () {
        colorStyleBtn.SetActive (settings.isColored);
        bwStyleBtn.SetActive (!settings.isColored);
    }
}