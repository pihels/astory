﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : Singleton<MenuManager> {

    public GameObject menuPanel;
    public GameObject buttonsPanel;
    public GameObject settingsPanel;
    public Animator popiAnimator;
    public Animator popiIdleAnimator;

    protected override void OnAwake () {
        bool hasStarted = AppManager.Instance == null || AppManager.Instance.hasStarted;
        menuPanel.SetActive (true);
        settingsPanel.SetActive (false);
        buttonsPanel.SetActive (hasStarted);
        popiAnimator.gameObject.SetActive (!hasStarted);
        popiIdleAnimator.gameObject.SetActive (hasStarted);
    }

    public void GoToStories () {
        AppManager.Instance.LoadScene ("StoriesLibrary");
    }

    public void GoToUserStories () {
        AppManager.Instance.LoadScene ("UserStoriesLibrary");
    }
    
    public void GoToTutorialVideo () {
        AppManager.Instance.LoadScene ("TutorialVideo");
    }

    public void GoToSettings () {
        menuPanel.SetActive (false);
        settingsPanel.SetActive (true);
    }

    public void GoToMenu () {
        menuPanel.SetActive (true);
        settingsPanel.SetActive (false);
    }

    public void ShowMenuButtons () {
        buttonsPanel.SetActive (true);
    }

    public void ShowIdlePopi () {
        popiIdleAnimator.gameObject.SetActive (true);
        popiAnimator.gameObject.SetActive (false);
    }

}