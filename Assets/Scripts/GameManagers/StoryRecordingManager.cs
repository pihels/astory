﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class StoryRecordingManager : MonoBehaviour {
    public AudioSource audioSource;
    public GameObject listenButton;
    public GameObject recordButton;
    public GameObject leftButton;
    public GameObject rightButton;
    public GameObject confirmButton;
    public RecordingHelper recordingHelper;
    public AnimationPlayer player;
    public GameObject dimmer;
    public PopiPopupSpeak popi;
    public AudioClip recordingStartClip;

    private bool hasRecordPermission = false;
    private bool hasStarted;

    void Awake () {
        recordButton.GetComponent<Animator> ().SetTrigger ("DoPopup");
        player.Init (false);
        player.PlayNextAnimation ();
        ToggleButtons ();
    }

    IEnumerator Start () {
        hasRecordPermission = Application.HasUserAuthorization (UserAuthorization.Microphone);
        recordButton.GetComponent<Button> ().interactable = hasRecordPermission;
        if (hasRecordPermission) {
            Debug.Log ("ASTORY Microphone permission already granted");
            yield return null;
        }

        Debug.Log ("ASTORY Waiting for recording permission");
        yield return Application.RequestUserAuthorization (UserAuthorization.Microphone);

        if (Application.HasUserAuthorization (UserAuthorization.Microphone)) {
            hasRecordPermission = true;
            recordButton.GetComponent<Button> ().interactable = true;
            Debug.Log ("ASTORY Microphone permission granted");
        } else {
            Debug.Log ("ASTORY Microphone permission not granted");
        }
    }

    private void ToggleButtons () {
        bool hasAudio = AppManager.Instance.currentStory.data[player.GetCurrentAnimationIndex ()].audio != null;
        leftButton.SetActive (!recordingHelper.isRecording && !player.IsFirstAnimation ());
        rightButton.SetActive (!recordingHelper.isRecording && !player.IsLastAnimation () && hasAudio);
        listenButton.SetActive (!recordingHelper.isRecording && hasAudio);
        confirmButton.SetActive (!recordingHelper.isRecording && player.IsLastAnimation () && hasAudio);
    }

    public void Record () {
        if (!hasStarted) {
            recordButton.GetComponent<Animator> ().SetBool ("DoPopup", false);
            popi.Hide ();
            dimmer.SetActive (false);
        }

        Debug.Log ("ASTORY Toggled record button");
        if (!recordingHelper.isRecording) {
            StopAudioPlayback ();
            StartCoroutine (StartRecordingWithDelay ());
        } else {
            recordButton.GetComponent<Image> ().color = Color.white;
            AppManager.Instance.currentStory.data[player.GetCurrentAnimationIndex ()].audio = recordingHelper.EndRecording ();
        }
        ToggleButtons ();
    }

    public void PlayAudio () {
        if (recordingHelper.isRecording) {
            return;
        }

        if (audioSource.isPlaying) {
            StopAudioPlayback ();
            return;
        }

        int idx = player.GetCurrentAnimationIndex ();
        if (AppManager.Instance.currentStory.data[idx].audio != null) {
            audioSource.clip = AppManager.Instance.currentStory.data[idx].audio;
            audioSource.Play ();
            listenButton.GetComponent<Image> ().color = Color.green;
            Invoke ("StopAudioPlayback", audioSource.clip.length);
        }
    }

    private void StopAudioPlayback () {
        audioSource.Stop ();
        listenButton.GetComponent<Image> ().color = Color.white;
    }

    public void ConfirmRecordings () {
        AppManager.Instance.currentStory.isFinished = true;
        AppManager.Instance.userLibrary.AddStory (AppManager.Instance.currentStory);
        AppManager.Instance.SaveData ();
        AppManager.Instance.LoadScene ("StoryAnimationWithRecordingScene");
    }

    public void Next () {
        player.PlayNextAnimation ();
        ToggleButtons ();
        StopAudioPlayback ();
    }

    public void Prev () {
        player.PlayPrevAnimation ();
        ToggleButtons ();
        StopAudioPlayback ();
    }

    private IEnumerator StartRecordingWithDelay () {
        float delay = 0.0f;
        if (recordingStartClip != null) {
            delay = 0.5f;
            audioSource.PlayOneShot (recordingStartClip);
        }

        yield return new WaitForSeconds (delay);
        recordingHelper.StartRecording ();
        recordButton.GetComponent<Image> ().color = Color.red;
    }
}