﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
public class StoryAnimationManager : MonoBehaviour {
    public AnimationPlayer player;
    public GameObject pauseButton;
    public GameObject playButton;
    public GameObject dimmer;
    public PopiPopupSpeak popi;
    public bool isWithRecordings;
    public string nextScene;

    private bool hasStarted;

    void Awake () {
        AppManager.Instance.DisableBackground ();
        player.Init (isWithRecordings);
        if (popi != null) {
            playButton.GetComponent<Animator> ().SetBool ("DoPopup", true);
        } else {
            Play ();
        }

    }

    private void AutoPlayAnimations () {
        if (player.IsLastAnimation ()) {
            Invoke ("AnimationEnded", 2.0f);
            return;
        }

        player.PlayNextAnimation ();

        float waitTime = player.GetCurrentSceneLength ();
        Invoke ("AutoPlayAnimations", waitTime);
    }

    private void AnimationEnded () {
        AppManager.Instance.LoadScene (nextScene);
    }

    public void Play () {
        if (!hasStarted) {
            playButton.GetComponent<Animator> ().SetBool ("DoPopup", false);
            if (popi != null) {
                popi.Hide ();
                dimmer.SetActive (false);
            }
        }

        hasStarted = true;
        pauseButton.SetActive (true);
        playButton.SetActive (false);
        player.ResumeAnimation ();

        float waitTime = player.GetCurrentSceneRemainingLength ();
        Invoke ("AutoPlayAnimations", waitTime);
    }

    public void Pause () {
        pauseButton.SetActive (false);
        playButton.SetActive (true);
        player.PauseAnimation ();

        CancelInvoke ();
    }

}