﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class AppManager : Singleton<AppManager> {
    public GameObject generalPanel;
    public GameObject backButton;
    public GameObject homeButton;
    public GameObject colorButton;
    public GameSettings currentSettings;
    public UserStoriesLibrary userLibrary;
    public StoryTemplatesLibrary templatesLibrary;
    public UserStory currentStory;
    public StoryTemplate currentTemplate;
    private List<string> sceneHistory = new List<string> ();

    public bool hasStarted;

    protected override void OnAwake () {
        userLibrary = LoadData ();
    }

    IEnumerator Start () {
        Debug.Log ("ASTORY Waiting for recording permission");
        yield return Application.RequestUserAuthorization (UserAuthorization.Microphone);

        if (Application.HasUserAuthorization (UserAuthorization.Microphone)) {
            Debug.Log ("ASTORY Microphone permission granted");
        } else {
            Debug.Log ("ASTORY Microphone permission not granted");
        }
    }

    void Update () {
        if (Input.GetKeyDown (KeyCode.Escape)) {
            Quit ();
        }
    }

    public void DisableBackground () {
        generalPanel.GetComponent<Image> ().enabled = false;
    }

    public void Quit () {
        Debug.Log ("ASTORY quit");
        Application.Quit ();
    }

    public void GoBack () {
        int lastIdx = sceneHistory.Count - 1;
        string prevScene = sceneHistory[lastIdx];
        sceneHistory.RemoveAt (lastIdx);
        LoadScene (prevScene, true);
    }

    public void ToggleColorMode () {

    }

    public void GoToMenu () {
        LoadScene ("Menu");
    }

    public void LoadScene (string sceneName, bool isBack = false) {
        generalPanel.SetActive (!String.Equals (sceneName, "Menu") && !String.Equals (sceneName, "TutorialVideo"));

        if (!isBack) {
            sceneHistory.Add (SceneManager.GetActiveScene ().name);
        }

        generalPanel.GetComponent<Image> ().enabled = true;
        SceneManager.LoadScene (sceneName);
    }

    public void SetCurrentStory (UserStory story, StoryTemplate template) {
        currentStory = story;
        currentTemplate = template; //GetTemplate (story.templateId);
    }

    public void SetCurrentStory (UserStory story) {
        currentStory = story;
        currentTemplate = GetTemplate (story.templateId);
    }

    public void SaveCurrentStory () {
        userLibrary.UpdateStory (currentStory);
    }

    public void SaveData () {
        Debug.Log ("ASTORY Saving data to " + Application.persistentDataPath);

        int i = 0;
        foreach (var story in userLibrary.stories) {
            int j = 0;
            foreach (var storyData in story.data) {
                string fileName = "astory_story" + i + "_scene" + j;
                storyData.audioFileName = SaveWavFile (storyData.audio, fileName);
                j++;
            }
            i++;
        }

        string json = JsonUtility.ToJson (userLibrary);
        File.WriteAllText (Application.persistentDataPath + Path.DirectorySeparatorChar + "UserStoriesLibrary.txt", json);

        Debug.Log ("ASTORY Data saved");
    }

    UserStoriesLibrary LoadData () {
        Debug.Log ("ASTORY Loading data");

        UserStoriesLibrary data = null;
        if (File.Exists (Application.persistentDataPath + Path.DirectorySeparatorChar + "UserStoriesLibrary.txt")) {
            data = ScriptableObject.CreateInstance<UserStoriesLibrary> ();
            string json = File.ReadAllText (Application.persistentDataPath + Path.DirectorySeparatorChar + "UserStoriesLibrary.txt");
            JsonUtility.FromJsonOverwrite (json, data);

            foreach (var story in data.stories) {
                foreach (var storyData in story.data) {
                    storyData.audio = LoadWavFile (storyData.audioFileName);
                }
            }

        } else {
            data = Resources.Load<UserStoriesLibrary> ("UserStoriesLibrary");
        }

        Debug.Log ("ASTORY Data loaded");
        return data;
    }

    public string SaveWavFile (AudioClip audioClip, string fileName) {
        if (audioClip == null) {
            return null;
        }
        string filepath;
        byte[] bytes = WavUtility.FromAudioClip (audioClip, out filepath, fileName, true);
        return filepath;
    }

    public AudioClip LoadWavFile (string filename) {
        if (String.IsNullOrEmpty (filename)) {
            return null;
        }

        string path = string.Format ("{0}/{1}", Application.persistentDataPath, filename);
        Debug.Log (filename);
        return WavUtility.ToAudioClip (filename);
    }

    public StoryAssetBundle GetAssetBundle (int id) {
        AssetBundle.UnloadAllAssetBundles (true);
        foreach (StoryAssetBundle bundle in templatesLibrary.bundles) {
            if (bundle.id == id) {
                return bundle;
            }
        }
        Debug.LogError ("Failed to find assetbundle conf with id! " + id);
        return null;
    }

    public StoryTemplate GetTemplate (int id) {
        StoryAssetBundle bundle = GetAssetBundle (id);
        if (bundle == null) {
            return null;
        }

        AssetBundle localAssetBundle = AssetBundle.LoadFromFile (Path.Combine (Application.streamingAssetsPath, bundle.name));
        if (localAssetBundle == null) {
            Debug.LogError ("Failed to load AssetBundle! " + bundle.name);
        }

        StoryTemplate template = localAssetBundle.LoadAsset<StoryTemplate> ("StoryTemplate");
        localAssetBundle.Unload (false);
        return template;
    }
}