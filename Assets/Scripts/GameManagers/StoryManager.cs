﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class StoryManager : Singleton<StoryManager> {
/*
    public GameObject gameCanvas;
    public GameObject recordCanvas;
    public GameObject storyCanvas;

    public AudioSource audioSource;
    public RectTransform dropAreas;

    public GameObject leftBtn;
    public GameObject rightBtn;
    public GameObject playStoryBtn;

    public RectTransform leftImageHolder;
    public RectTransform middleImageHolder;
    public RectTransform rightImageHolder;
    public GameObject imagePrefab;
    public GameObject dropAreaPrefab;
    public StoryData defaultStory;
    private List<Draggable> storyPieces;
    private List<Draggable> remainingPieces;
    private bool isStoryOrdered;
    private int curMidIdx = 1;

    void Start () {
        GoToGameScreen ();
        InitStoryElements (defaultStory);
        InitUI ();
    }

    private void InitStoryElements (StoryData story) {
        storyPieces = new List<Draggable> ();
        remainingPieces = new List<Draggable> ();

        int i = 1;
        foreach (StoryPiece piece in story.pieces) {
            GameObject imageGo = GameObject.Instantiate (imagePrefab);
            Draggable draggable = imageGo.GetComponent<Draggable> ();
            draggable.index = piece.index;
            draggable.storyPiece = piece;

            Sprite image = GetStoryImage (piece);
            draggable.ghostGo.GetComponent<Image>().sprite = image;
            draggable.imageGo.GetComponent<Image>().sprite = image;
            storyPieces.Add (draggable);
            remainingPieces.Add (draggable);

            GameObject dropAreaGo = GameObject.Instantiate (dropAreaPrefab, dropAreas);
            DropArea dropArea = dropAreaGo.GetComponent<DropArea> ();
            dropArea.index = piece.index;
            Text dropAreaText = dropAreaGo.GetComponentInChildren<Text>();
            dropAreaText.text = i.ToString();
            i++;
        }

        remainingPieces.Sort ((a, b) => Random.Range (0, 2) * 2 - 1);
    }

    private void InitUI () {
        if (remainingPieces.Count == 0) {
            return;
        }

        foreach (Draggable piece in remainingPieces) {
            if (!piece.GetComponent<Draggable> ().isLocked) {
                piece.transform.parent = null;
                piece.gameObject.SetActive (false);
            }
        }

        if (curMidIdx > 0) {
            SetHolderImage (leftImageHolder, remainingPieces[curMidIdx - 1]);
        }

        SetHolderImage (middleImageHolder, remainingPieces[curMidIdx]);

        if (curMidIdx < remainingPieces.Count - 1) {
            SetHolderImage (rightImageHolder, remainingPieces[curMidIdx + 1]);
        }

    }

    private void SetHolderImage (RectTransform imageHolder, Draggable storyPiece) {
        storyPiece.gameObject.SetActive (true);
        SetAndStretchToParentSize (storyPiece.GetComponent<RectTransform> (), imageHolder);
    }

    private Sprite GetStoryImage (StoryPiece piece) {
        GameSettings settings = SettingsManager.Instance.Settings;
        if (settings.isColored) {
            return piece.imageColor;
        }

        return piece.imageBlackWhite;
    }

    public void GoToRecordScreen (GameObject imageGo, StoryElementData elementData) {
        if (!isStoryOrdered) {
            return;
        }

        gameCanvas.SetActive (false);
        recordCanvas.SetActive (true);
        storyCanvas.SetActive (false);

        RecordManager.Instance.Init (imageGo, elementData);
    }

    public void GoToGameScreen () {
        gameCanvas.SetActive (true);
        recordCanvas.SetActive (false);
        storyCanvas.SetActive (false);
    }

    public void GoToStoryScreen () {
        gameCanvas.SetActive (false);
        recordCanvas.SetActive (false);
        storyCanvas.SetActive (true);

        List<StoryPiece> pieces = new List<StoryPiece> ();
        foreach (Draggable piece in storyPieces) {
            pieces.Add (piece.storyPiece);
        }
        StoryPlaybackManager.Instance.StartStoryPlayback (pieces);
    }

    public bool IsStoryOrdered () {
        return remainingPieces.Count == 0;
    }

    public void PlayAudio (AudioClip audio) {
        audioSource.clip = audio;
        audioSource.Play ();
    }

    public void ChangeMiddleImageIndex (int i) {
        curMidIdx = Mathf.Clamp (curMidIdx + i, 0, remainingPieces.Count - 1);
        InitUI ();
    }

    public void StoryPieceLocked (Draggable piece) {
        remainingPieces.Remove (piece);
        ChangeMiddleImageIndex (0);
        isStoryOrdered = IsStoryOrdered ();

        if (isStoryOrdered) {
            leftBtn.SetActive (false);
            rightBtn.SetActive (false);
            playStoryBtn.SetActive (true);
        }
    }

    //UTIL METHODS, move to another class
    public static void RandomizeSiblingOrder (Transform parent) {
        int childCount = parent.childCount;
        foreach (Transform child in parent) {
            int idx = Random.Range (0, childCount);
            child.SetSiblingIndex (idx);
        }
    }

    public static void SetAndStretchToParentSize (RectTransform _mRect, RectTransform _parent) {
        _mRect.transform.SetParent (_parent);
        _mRect.localScale = Vector3.one;
        _mRect.localPosition = Vector3.zero;
        _mRect.sizeDelta = new Vector2 (0.0f, 0.0f);
    }
    */
}