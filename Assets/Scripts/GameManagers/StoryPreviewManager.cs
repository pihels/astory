﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class StoryPreviewManager : MonoBehaviour
{
    public GameObject elementTemplate;
    public RectTransform imageHolder;
    public GameObject leftBtn;
    public GameObject rightBtn;
    public GameObject continueBtn;
    public GameObject continueText;
    public GameObject popiText;
    private int curImageIdx = 0;
    private List<DraggableStoryElement> elements = new List<DraggableStoryElement>();
    private Vector3 dragStart;
    private bool hasFinished;

    void Awake()
    {
        ImageHelper.InitStoryElements(AppManager.Instance.currentTemplate, elementTemplate, elements);
        ImageHelper.ShuffleElements(elements);
        foreach (DraggableStoryElement el in elements)
        {
            ImageHelper.SetAndStretchToParentSize(el.GetComponent<RectTransform>(), imageHolder);
        }

        continueBtn.SetActive(false);
        continueText.SetActive(false);
        InitUI();
    }

    private void InitUI()
    {
        int i = 0;
        foreach (DraggableStoryElement el in elements)
        {
            el.gameObject.SetActive(i == curImageIdx);
            i++;
        }

        leftBtn.SetActive(curImageIdx > 0);
        rightBtn.SetActive(curImageIdx < (elements.Count - 1));

        if (curImageIdx >= elements.Count - 1)
        {
            hasFinished = true;
            continueBtn.GetComponent<Animator>().SetBool("DoPopup", true);
        }

        continueBtn.SetActive(hasFinished);
        continueText.SetActive(hasFinished);
        popiText.SetActive(!hasFinished);
    }

    public void ChangeMiddleImageIndex(int i)
    {
        curImageIdx = Mathf.Clamp(curImageIdx + i, 0, elements.Count - 1);
        InitUI();
    }

    public void Continue()
    {
        AppManager.Instance.LoadScene("StoryGameScene");
    }

    public void OnBeginDrag()
    {
        dragStart = Input.mousePosition;
    }

    public void OnEndDrag()
    {
        Vector3 dir = dragStart - Input.mousePosition;
        Debug.Log(dir);
        if (dir.x > 100)
        {
            ChangeMiddleImageIndex(1);
        }
        else if (dir.x < 100)
        {
            ChangeMiddleImageIndex(-1);
        }
    }
}