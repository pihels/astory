﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (fileName = "Settings", menuName = "AStory/Settings", order = 0)]
public class GameSettings : ScriptableObject {
    public bool isColored = true;
    public bool showButtons = true;
}