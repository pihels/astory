﻿using System.Collections;
using UnityEngine;

public class PopiPopupSpeak : MonoBehaviour {
    private const float DEFAULT_DURATION = 3.0f;
    public GameObject content;
    public Animator popiAnimator;
    public AudioClip clip;
    public AudioSource audioSource;
    public bool hideAfterSpeaking;

    void Start () {
        StartSpeaking ();
    }

    public void Hide () {
        StopSpeaking ();
        content.SetActive (false);
    }

    public void StartSpeaking () {
        popiAnimator.SetBool ("IsActive", true);
        float duration = DEFAULT_DURATION;
        if (clip != null) {
            duration = clip.length;
            audioSource.clip = clip;
            audioSource.Play ();
        }
        Invoke ("StopSpeaking", duration);
    }

    public void StopSpeaking () {
        popiAnimator.SetBool ("IsActive", false);
        audioSource.Stop ();

        if (hideAfterSpeaking) {
            content.SetActive (false);
        }
    }
}